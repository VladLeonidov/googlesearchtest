package com.leus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleSearchTest {

    private WebDriver chromeDriver;
    private String startUrl;

    @BeforeClass
    private void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:/browsersDrivers/chromedriver.exe");
        chromeDriver = new ChromeDriver();
        chromeDriver.manage().window().maximize();
        chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        startUrl = "https://www.google.com/";
    }

    @Test
    public void searchTest() {
        chromeDriver.get(startUrl);
        WebElement searchField = chromeDriver.findElement(By.id("lst-ib"));
        searchField.sendKeys("selenium web driver");
        WebElement searchButton = chromeDriver.findElement(By.xpath("//li[@class='gsfs']//input[@class='lsb']"));
        searchButton.click();
        List<WebElement> resultsOfSearch = chromeDriver.findElements(By.xpath("//div[@class='mw']//div[@id='rso']//div[@class='srg']/div"));
        Assert.assertEquals(resultsOfSearch.size(), 10);
    }

    @AfterClass
    private void shutDown() {
        if (chromeDriver != null) {
            chromeDriver.quit();
        }
    }
}
